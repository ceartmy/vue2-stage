import { initState } from './initState';
import { compileToFunction } from "./compiler/index";
import { mountComponent } from "./lifecycle";

export function initMixin(Vue) {
  Vue.prototype._init = function (options) { // 用于初始化操作
    // console.log(options);
    // vm、this 都是Vue实例
    const vm = this;
    vm.$options = options; // 将用户的选项挂载到实例上

    // 初始化状态
    initState(vm);
  }

  // $mount函数 传入element 生成render函数
  Vue.prototype.$mount = function (el) { // 用于初始化操作
    const vm = this;
    el = document.querySelector(el);
    let ops = vm.$options; // 将用户的选项挂载到实例上
    if (!ops.render) {
      let template = ops.template;
      if (!template) {
        template = el.outerHTML;
      }

      // 将template变成render函数
      // 创建render函数 -》 虚拟dom -》 渲染真实dom
      const render = compileToFunction(template);// 开始编译
      ops.render = render;
    }
    
    mountComponent(vm, el);
  }
}



import { parseHTML, genCode } from "./parse";


// 将模板变成render函数 通过 with + new Function的方式让字符串变成js语法来执行
export function compileToFunction(template) {
    // 获取ast语法树
    let ast = parseHTML(template);
    
    // 通过ast语法树转换成render函数
    let code = genCode(ast);
    
    // 生成render函数 将字符串变成函数
    const render = new Function(`with(this){return ${code}}`);

    return render;
}






// 将template转换成ast语法树 -> 再将语法树转换成一个字符串拼接在一起

// ast 使用来描述语言本身的，语法中没有的不会描述出来，不能自己增添属性
/*let obj = {
    tag,
    attrs,
    context,
    component
}*/
// vdom 虚拟dom 描述真实dom元素的，可以自己增添属性
/*
let obj = {name:'lg',age:21};
function render() {
    with(this) {
        console.log(name);
        console.log(age);
    }
}
render.call(obj);
*/

/*(function anonymous(
) {
    with(this){
        return
        _c('div',
            {id:"app",style:{"width":" 100px","height":" 100px"},disabled:true},
            _v(_s(msg)),
            _c('h1',undefined,_v(_s(123)+",world.")),
            _c('span',undefined,_v("123"),_c('i',undefined,_v("456")))
        )
    }
})*/
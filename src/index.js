import { initMixin } from "./init"
import {lifeCycleMixin} from "./lifecycle";



function Vue(options) {
  // debugger;
  this._init(options);
}


// 初始化模块
initMixin(Vue);

lifeCycleMixin(Vue);


export default Vue
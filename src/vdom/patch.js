
export function patch(oldVnode, vnode) {// oldVnode 可能是后续两个虚拟节点的比较
  const isRealElement = oldVnode.nodeType;// 如果有说明是一个真实元素
  if (isRealElement) {
      const oldElm = oldVnode;
      // 需要获取父节点 将当前节点的下一个元素作为参照物 将他插入 之后删除旧节点
      const parentNode = oldElm.parentNode;// 父节点
      // 根据虚拟节点替换
      let el = createElm(vnode);
      parentNode.insertBefore(el, oldElm.nextSibling);
      parentNode.removeChild(oldElm);
      return el;
  }
  // diff算法
}

function createElm(vnode) {
  let {tag, data, children, text} = vnode;
  if (typeof tag === 'string') {// 元素
      vnode.el = document.createElement(tag);
      // 后续我们需要diff算法 拿虚拟节点比对后更新dom
      for (let i=0,length=children.length; i<length; i++) {
          vnode.el.appendChild(createElm(children[i])); // 递归渲染
      }
  } else { // 文本
      vnode.el = document.createTextNode(text);
  }
  // 从根虚拟节点创建真是节点
  return vnode.el;
}

// 每次更新页面的话 dom结构是不会变的
// 调用render方法时，数据变化了，渲染成新的虚拟节点vnode，渲染dom
import { observer } from './observe/index';

export function initState(vm) {
  const opts = vm.$options;
  if (opts.data) {
    initData(vm);// 初始化数据 监听器等
  }
  
}

// vue2 对data初始化 有几种情况可能是函数或对象
export function initData(vm) {
  let { data } = vm.$options;

  // 如果data是函数就call执行函数，对象就返回data或者空对象初始值 这时候可以vm._data.xxx拿到数据，data放在私有属性_data上
  data = vm._data = typeof data === 'function' ? data.call(this) : data || {};

  // 将data里面的每个数据重新代理 这时候可以vm.xxx拿到数据
  for (const key in data) {
    proxy(vm, '_data', key);
  }
  // 数据拦截 修改数据后监听到
  observer(vm._data);
}

// 在vm实现上动态添加所有属性 vm.xxx => vm._data.xxx
function proxy(vm,target,key){
  Object.defineProperty(vm,key,{
      get(){
          return vm[target][key];
      },
      set(newVal){
          vm[target][key] = newVal;
      }
  })
}
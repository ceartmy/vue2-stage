// 重写数组方法

// (1) 获取原来的数组方法
let oldArrayProtoMethods = Array.prototype;

// (2) 继承 生成新的数组对象，继承原来的属性
let ArrayMethods = Object.create(oldArrayProtoMethods);

// (3) 函数劫持 数组方法
let methods = [
    'push',
    'pop',
    'unshift',
    'shift',
    'splice',
    'sort',
    'reverse'
];

// 遍历要劫持的方法
methods.forEach(method => {
    // 对方法进行改造
    ArrayMethods[method] = function (...args) {
        let inserted;
        let ob = this.__ob__;
        // 如果是对数组进行增加或插入
        switch (method) {
            case 'push':
            case 'unshift':
                inserted = args; // 数组
                break;
            case 'splice': // arr.splice(1,1,xxx)
                inserted = args.slice(2);
                break;
            default:
                break;
        }
        if (inserted) {
            // 对新增的数据重新进行观测
            ob.observeArray(inserted);
        }
        // 执行本来的数组方法
        let result = oldArrayProtoMethods[method].call(this,...args);
        // let result = oldArrayProtoMethods[method].apply(this,args);
        return result;
    }
});

export default ArrayMethods;
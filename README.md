yarn add -D rollup rollup-plugin-babel @babel/core @babel/preset-env

## promise 解决无限嵌套
```
pending 等待状态
pending 成功
rejected 失败
Promise 的构造器接收一个执行函数 (executor)，可以在这个执行函数里手动地 resolve 和 reject 一个 Promise

初始pending，resolve()转变为成功状态，reject()为失败状态
状态一旦改变不可更改
resolve(xx)数据会传给then(成功，失败)成功回调函数
reject(xx) 数据会传给then中 的失败回调函数方法

成功状态调用成功方法resolve，失败状态调用失败方法 reject

需要把then中的成功失败回调保存,状态改变时执行存起来的成功失败回调
```

## vue响应式 原理
```js
Observer（数据监听器） : 
Observer的核心是通过Object.defineProprtty()来监听数据的变动，
这个函数内部可以定义setter和getter，每当数据发生变化，就会触发setter。
这时候Observer就要通知订阅者，订阅者就是Watcher

Watcher（订阅者） :
 Watcher订阅者作为Observer和Compile之间通信的桥梁，
 主要做的事情是：
在自身实例化时往属性订阅器(dep)里面添加自己
自身必须有一个update()方法
待属性变动dep.notice()通知时，能调用自身的update()方法，并触发Compile中绑定的回调


Compile（指令解析器） : 
Compile主要做的事情是解析模板指令，将模板中变量替换成数据，
然后初始化渲染页面视图，并将每个指令对应的节点绑定更新函数，
添加鉴定数据的订阅者，一旦数据有变动，收到通知，更新试图
```
## vue-router
```js
步骤
 * 1.在VueRouter中创建$router(当前路由实例) $route（当前路由）
 * 2.初始化页面模式mode history和hash
 * 3.根据routes创建路由表 path -> compunent
 * 4.根据router-link展示路由路径
 * 5.根据router-view去选择对应path的componuet
```
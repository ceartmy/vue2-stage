import babel from 'rollup-plugin-babel';
import resolve from '@rollup/plugin-node-resolve';
export default {
  input: './src/index.js', // 入口
  output: {
    file: './dist/vue.js', // 出库
    name: 'Vue',
    format: 'umd', // esm es6模块 comonjs模块 iife自执行函数 umd(comonjs amd)
    sourcemap: true, 
  },
  Plugins:[
    babel({
      exclude: 'node_modules/**'
    }),
    resolve()
  ]
}